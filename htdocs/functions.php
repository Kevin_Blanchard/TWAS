<?php

class Init {
    private $array_projects;
    public  $count_projects;

    public function __construct() {
        // load list for menu
        $content = utf8_encode(file_get_contents('projects.xml'));
        $this->array_projects = simplexml_load_string($content);
    }

    /* function for create menu */
    public function sidebarLeft() {
        foreach ($this->array_projects->projects->project as $project) {
            echo '
                <li class="pure-menu-item" style="border-left: 3px solid '.$project->color.';">
                    <a href="index.php?q='.$project->url.'" class="pure-menu-link">'.$project->name.'</a>
                </li>';
            $this->count_projects++;
        }
    }

    /* function for include iframe in page */
    public function iframeIntegration() {
        if(isset($_GET['q'])) {
            echo '<div>';
            echo '<iframe id="iframe" src="projects/'.$_GET['q'].'" width="100%" onload="resizeIframe(this)"></iframe>';
            echo '</div>';
        }
    }

    public function htmlBoite() {
        foreach ($this->array_projects->projects->project as $project) {
            echo '<div style="pure-button">';
            echo '<p><a class="pure-button pure-button-primary" href="index.php?q='.$project->url.'" style="border-left: 3px solid '.$project->color.';">
                    '.$project->name.'
                    <br><span>'.$project->desc.'</span>
                </a></p>';
            echo '</div>';
        }
    }
}

?>
