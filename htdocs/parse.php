<div id="layout">
    <a href="#menu" id="menuLink" class="menu-link"></a>
    <div id="menu">
        <div class="pure-menu">
            <a class="pure-menu-heading" href=".">TWAS</a>
            <ul class="pure-menu-list">
                <?php $init->sidebarLeft(); ?>
            </ul>
        </div>
    </div>

    <script language="javascript" type="text/javascript">
        function resizeIframe(obj) {
            obj.style.height = window.innerHeight + 'px';
        }
    </script>

    <div id="main">
        <?php
            if (isset($_GET['q'])) {
                if ($_GET['q'] !== '') {
                    $init->iframeIntegration();
                }
            }
            else {
                include('start.php');
            }
        ?>
    </div>
</div>
