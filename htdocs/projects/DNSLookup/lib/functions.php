<?php

function dns_request($dns_server_index, $domain, $type, $ipv) {
	$objResponse = new xajaxResponse();
	$dns_servers = mySfYaml::get('dns.servers');
	$keys        = array_keys($dns_servers);
	$dns_server  = $dns_servers[$keys[$dns_server_index]];
	$servers     = array();

	$ipv     = in_array($ipv, array('4', '6')) ? $ipv : '4';
	$type    = in_array($type, mySfYaml::get('dns.types')) ? $type : 'A';
	$domain  = escapeshellarg($domain); 

	foreach($dns_server['servers'] as $dns) {
		$command = 'host -W 2 -t '.$type.' -'.$ipv.' '.$domain.' '.escapeshellarg($dns);
		$hosts   = shell_exec($command);

		if($hosts) {
			$lines = explode("\n", $hosts);
			$empty = false;

			foreach($lines as $k => $v) {
				if(!$empty) {
					if(trim($v) == "") {
						unset($lines[$k]);
						$empty = true;
					}
					else {
						unset($lines[$k]);
					}
				}

				if(trim($v) == "") {
					unset($lines[$k]);
				}
			}

			$servers[] = array(
				'dns'   => $dns, 
				'datas' => nl2br(htmlentities(implode("\n", $lines)))
			);
		}
	}

	$datas = array(
		'title'   => $dns_server['title'],
		'logo'    => $dns_server['logo'],
		'servers' => $servers
	);

	$json_datas = json_encode($datas);

	$objResponse->script('add_dns_result('.$json_datas.');');
	$objResponse->script("update();");

	return $objResponse;
}

function whois_request($domain) {
	$objResponse = new xajaxResponse();
	$domain  = escapeshellarg($domain); 
	$command = 'whois '.$domain;
	$whois   = shell_exec($command);

	$json_datas = json_encode(array(
		'title' => 'Whois de '.htmlentities(str_replace("'", '', $domain)),
		'datas' => implode('<br />', explode("\n", $whois))
	));

	$objResponse->script('add_whois_result('.$json_datas.');');
	$objResponse->script("end(true);");
	return $objResponse;

}

XajaxSg::getInstance()->registerFunction('dns_request');
XajaxSg::getInstance()->registerFunction('whois_request');
