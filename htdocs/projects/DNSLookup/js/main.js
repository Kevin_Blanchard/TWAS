var add_dns_result = function(obj) {
	if(obj) {	
		var title = obj.title;
		var logo  = 'img/fai/'+obj.logo;

		var html = '<div class="dns_result">';
			html+= '<h2>'+title+' <img src="'+logo+'" title="'+logo+'" alt="'+logo+'" /></h2>';

			if(!obj.servers.length) {
				html+= '<div class="alert alert-warning">Echec</div>';
			}
			else {
				html+= '<table class="table table-striped table-bordered table-condensed">';
				
				$(obj.servers).each(function(i, v) {
					html+= '<tr>';
						html+= '<th>'+v.dns+'</th>';
						
						if(!v.datas) {
							html+= '<td><i class="icon-exclamation-sign"></i> Aucun réponse</td>';
						}
						else {
							if(v.datas.indexOf('REFUSED') != -1) {
								v.datas = '<i class="icon-exclamation-sign"></i> '+v.datas;
							}
							html+= '<td>'+v.datas+'</td>';
						}
					html+= '</tr>';
				});
			}

			html+= '</div>';
		html+= '</div>';

		$('#results').append(html);
	}
}

var add_whois_result = function(obj) {
	if(obj) {	
		var title = obj.title;
		var logo  = 'img/fai/'+obj.logo;

		var html = '<div class="dns_result">';
			html+= '<h2>'+title+'</h2>';

			if(!obj.datas) {
				html+= '<div class="alert alert-warning">Echec</div>';
			}
			else {
				html+= '<table class="table table-striped table-bordered table-condensed">';
				
				html+= '<tr>';
					html+= '<td>'+obj.datas+'</td>';
				html+= '</tr>';
			}

			html+= '</div>';
		html+= '</div>';

		$('#results').append(html);
	}

}

var init = function(loading2) {
	if(!loading2) {
		$('#results').html('');
		$('#loading .bar').css('width', 0);
		$('#loading').fadeIn();
	}
	else {
		$('#results').html('');
		$('#loading2 .bar').css('width', 0);
		$('#loading2').fadeIn();
	}
}

var dns_done = 0;
var results = null;

var end = function(loading2) {
	if(!loading2) {
		$('#loading').fadeOut();
	}
	else {
		$('#loading2').fadeOut();
	}
}

var update = function() {
	dns_done++;
	
	$('#loading .bar').css('width', (100*dns_done/dns_length)+'%');

	if(dns_done == dns_length) {
		window.setTimeout(function() {
			end();
		}, 1000);
	}
}

var analyse = function() {
	var type   = $('#type').val();
	var ipv    = $('#type').val() == 1 ? '4' : '6';
	var domain = $('#domain').val().replace(/\s*/, '');

	if(!domain) {
		$('#domain').addClass('input_error');
		return false;
	}

	dns_done = 0;
	init();
	$('#domain').removeClass('input_error');
	
	for(var u=0; u<dns_length; u++) {
		xajax_dns_request(u, domain, type, ipv);
	};
}

var whois = function() {
	var domain = $('#domain').val().replace(/\s*/, '');

	if(!domain) {
		$('#domain').addClass('input_error');
		return false;
	}

	init(true);
	xajax_whois_request(domain);
}

$(function() {	
	$('#selection_types button').click(function() {
		$('#type').val($(this).val());
		$('#selection_types button').removeClass('btn-primary');
		$(this).addClass('btn-primary');
	});

	$('#selection_ipv button').click(function() {
		$('#ipv').val($(this).val());
		$('#selection_ipv button').removeClass('btn-primary');
		$(this).addClass('btn-primary');
	});

	$('#analyse').bind('click', analyse);
	$('#whois').bind('click', whois);
});
