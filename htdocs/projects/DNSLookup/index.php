<?php

require 'lib'.DIRECTORY_SEPARATOR.'bootstrap.php';

XajaxSg::getInstance()->processRequest();

?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript">
var dns_length = <?php echo count(mySfYaml::get('dns.servers')); ?>;
</script>
<script type="text/javascript" src="js/bootstrap-modal.js"></script>
<script type="text/javascript" src="js/main.js"></script>
<?php XajaxSg::getInstance()->printJavascript(); ?>
<script type="text/javascript" src="lib/xajax/xajax_js/xajax_core.js"></script>
<link rel="stylesheet" href="css/bootstrap.min.css" />
<link rel="stylesheet" href="css/main.css" />
<link rel="shortcut icon" href="favicon.ico" />
<title>DNS Lookup - Deblan.fr</title>
<body>

<div class="navbar navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
			<a class="brand" href="/">DNSLookup</a>
			<div class="nav-collapse">
				<ul class="nav">
					<li class="active"><a href="/">Accueil</a></li>
					<li><a href="../">Blog</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

<div id="main" class="container-fluid">
	<p id="description"><strong>DNSLookup</strong> de Deblan est un service en ligne qui vous permet instantanément d'<strong>effectuer une recherche DNS</strong> pour vérifier l'enregistrement DNS associé à un nom de domaine sur une sélection de serveurs de noms français. Ceci est particulièrement utile pour vérifier l'état actuel de la propagation des DNS après avoir modifié vos zones domaines.</p>

	<div class="row-fluid" id="form">
		<div class="btn-toolbar">
			<div>
				<input type="hidden" name="type" id="type" value="A" />
				<input type="hidden" name="ipv" id="ipv" value="1" />
				<input type="text" placeholder="Nom de domaine" id="domain" />

				<button class="btn btn-primary" id="analyse">Analyser</button>
				<button class="btn btn-primary" id="whois">Whois</button>

				<span class="btn-group" id="selection_types">
					<?php foreach(mySfYaml::get('dns.types') as $k => $type): ?>
						<button class="btn <?php echo $k == 0 ? 'btn-primary' : '' ?>" value="<?php echo $type; ?>"><?php echo $type; ?></button>
					<?php endforeach; ?>
				</span>

				<span class="btn-group" id="selection_ipv">
					<button class="btn btn-primary" value="1">IPV4</button>
					<button class="btn" value="2">IPV6</button>
				</span>
			</div>
		</div>
	</div>

	<div class="loading" id="loading">
		<h3>Chargement…</h3>
		<p>Veuillez patienter pendant que les requêtes DNS se font :)</p>
    <div class="progress progress-striped active">
			<div class="bar"></div>
		</div>
	</div>

	<div class="loading" id="loading2">
		<h3>Chargement…</h3>
		<p>Veuillez patienter un instant :)</p>
    <div class="progress progress-striped active">
			<div class="bar"></div>
		</div>
	</div>


	<div id="results"></div>

	<hr />
	<footer>
		<p>Service proposé par Simon Vieille - <a href="#about" data-toggle="modal">A propos</a></p>
	</footer>
</div>



<div class="modal hide" id="about">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">×</button>
		<h3>Télécharger les sources</h3>
	</div>

	<div class="modal-body">
		<p><strong>DNSLookup est un programme sous licence <a href="http://fr.wikipedia.org/wiki/Licence_BSD">BSD</a>.</strong>.</p>
		<p>Le <strong><a href="http://twitter.github.com/bootstrap/index.html">Boostrap Twitter</a></strong> est sous licence <strong><a href="http://www.apache.org/licenses/LICENSE-2.0">Apache License v2.0</a></strong>.</p>
		<p>Les icones sont de <strong><a href="http://glyphicons.com/">Glyphicons Free</a></strong>, sous licence <strong><a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a></strong>.</p>
		<p><strong>Xajax</strong> est sous licence <strong><a href="http://fr.wikipedia.org/wiki/Licence_BSD">BSD</a></strong>.</p>

		<code>git clone git://git.deblan.org/dns-lookup.git</code>
	</div>

	<div class="modal-footer">
		<a href="#" class="btn" data-dismiss="modal">Fermer</a>
	</div>
</div>

</body>
</html>
