# TWAS : Tools for Web Application Static

## Projects application

- Builder4Impress (MIT License)
- Harmony (MIT License)
- miniPaint (GPL)
- DNSLookup (BSD)
- Laverna (Mozilla Public License V2.0)
- regex-builder (BSD-2-Clause)
- MyCryptoChat (LGPL)
- QRcodeJS (MIT License)
- Comicgen (? licence : https://github.com/chambs/comicgen)
- Base64 (? licence : https://github.com/progers/base64)
- subnet-calculator (Manasseh Pierce : http://codepen.io/ManassehPierce/pen/oxWobq)

## Dépendances

- php5-sqlite (MyCryptoChat)

## Licence

- GNU GPL

## Author

- Kévin Blanchard (kevin.blanchard.in)
http://codepen.io/ManassehPierce/pen/oxWobq